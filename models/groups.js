'use strict';
module.exports = (sequelize, DataTypes) => {
    var Groups = sequelize.define('Groups', {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        team_id: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{1,5}$/,
            }
        },
        group: {
            type: DataTypes.STRING,
            is: /^[A-B]{1}$/
        },
        year: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{4}$/
            }
        }
    }, {
        freezeTableName: true,
        tableName: 'groups',
        timestamps: false,
    });
    Groups.associate = function (models) {
        Groups.belongsTo(models.Teams, {foreignKey: 'team_id', as: 'presence'});
    };

    return Groups;
};