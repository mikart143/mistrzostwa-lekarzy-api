'use strict';
module.exports = (sequelize, DataTypes) => {
    var Players = sequelize.define('Players', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        team_id: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{1,5}$/
            }
        },
        name: {
            type: DataTypes.STRING,
            validate: {
                is: /^[a-z0-9 żźćńółęąś]+$/i,
                notEmpty: true,
            }
        },
        role: {
            type: DataTypes.STRING,
            validate: {
                is: /^[a-zżźćńółęąś]+$/i,
                notEmpty: true,
            }
        },
        size: {
            type: DataTypes.STRING,
            validate: {
                is: /^[sxlm]{1,3}$/i,
                notEmpty: true,
            }
        }
    }, {
        freezeTableName: true,
        tableName: 'players',
        timestamps: false,
    });
    Players.associate = function (models) {
        Players.belongsTo(models.Teams, {foreignKey: 'team_id', as: 'players'});
    };

    return Players;
};