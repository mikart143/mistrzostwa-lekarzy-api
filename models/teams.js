'use strict';
module.exports = (sequelize, DataTypes) => {
    var Teams = sequelize.define('Teams', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            validate: {
                is: /^[a-z0-9 żźćńółęąś]+$/i,
                notEmpty: true,
            }
        },
        phone: {
            type: DataTypes.STRING,
            validate: {
                is: /^$|^[0-9\+]{9,12}$/
            }
        }
    }, {
        freezeTableName: true,
        tableName: 'teams',
        timestamps: false,
    });
    Teams.associate = function (models) {
        Teams.hasOne(models.Applications, {foreignKey: 'team_id', as: 'team'});
        Teams.hasMany(models.TeamPhotos, {foreignKey: 'team_id', as: 'photos'});
        Teams.hasMany(models.Groups, {foreignKey: 'team_id', as: 'presence'});
    };

    return Teams;
};