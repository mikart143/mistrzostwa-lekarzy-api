'use strict';

module.exports = (sequelize, DataTypes) => {
    var Applications = sequelize.define('Applications', {
        id: {type: DataTypes.INTEGER, primaryKey: true},
        team_id: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{1,5}$/
            }
        },
        paid: {
            type: DataTypes.BOOLEAN
            // validate: {
            //     is: /^[0-1]{1}$/
            // }
        },
        accepted: {
            type: DataTypes.BOOLEAN
        }
    }, {
        freezeTableName: true,
        tableName: 'applications',
        timestamps: false,
    });
    Applications.associate = function (models) {
        Applications.belongsTo(models.Teams, {foreignKey: 'team_id', as: 'team'});
        Applications.hasMany(models.Players, {foreignKey: 'team_id', as: 'players'});
    };

    return Applications;
};