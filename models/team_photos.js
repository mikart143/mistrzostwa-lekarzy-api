'use strict';
module.exports = (sequelize, DataTypes) => {
    var TeamPhotos = sequelize.define('TeamPhotos', {
        id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
        path: {
            type: DataTypes.STRING,
            validate: {
                is: /^[a-f0-9]{32}\.jpg$/i,
                notEmpty: true
            }
        },
        team_id: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{1,5}$/,
            }
        },
        year: {
            type: DataTypes.INTEGER,
            validate: {
                is: /^[0-9]{4}$/,
            }
        }
    }, {
        freezeTableName: true,
        tableName: 'team_photos',
        timestamps: false,
    });
    TeamPhotos.associate = function (models) {
        TeamPhotos.belongsTo(models.Teams, {foreignKey: 'team_id', as: 'photos'});
    };


    return TeamPhotos;
};