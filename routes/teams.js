var models = require('../models');
var express = require('express');
var router = express.Router();
var pluck = require('arr-pluck');
var fs = require('fs');
const fileType = require('file-type');
var photoStorage = './photos';
var photoSize = 6291456; //6 MB
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, photoStorage)
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

var upload = multer({
    storage: storage,
    limits: {fileSize: photoSize}
}).single('data');

// Get all teams
router.get('/', function (req, res, next) {
    models.Teams.findAll({
        include: [
            {model: models.TeamPhotos, as: 'photos', attributes: ['path', 'year']},
            {model: models.Groups, as: 'presence', attributes: ['year']}
        ],
        order: [
            ['id','DESC'],
            [{model: models.TeamPhotos, as: 'photos'}, 'year', 'ASC'],
            [{model: models.Groups, as: 'presence'}, 'year', 'ASC']
        ]
    }).then(function (data) {
        let _data = JSON.parse(JSON.stringify(data));
        for (i in _data) {
            _data[i].presence = pluck(_data[i].presence, "year");
        }
        res.status(200).send(_data);

    }).catch(function () {
        res.status(500).send();
    });
});
// Get one team
router.get('/:id', function (req, res, next) {
    models.Teams.find({
        where: {id: req.params.id},
        include: [
            {model: models.TeamPhotos, as: 'photos', attributes: ['path', 'year']},
            {model: models.Groups, as: 'presence', attributes: ['year']}
        ],
        order: [
            [{model: models.TeamPhotos, as: 'photos'}, 'year', 'ASC'],
            [{model: models.Groups, as: 'presence'}, 'year', 'ASC']
        ]
    }).then(function (data) {
        let _data = JSON.parse(JSON.stringify(data));
        _data.presence = pluck(_data.presence, "year");
        res.status(200).send(_data);
    }).catch(function () {
        res.status(404).send();
    });
});
// Update team
router.put('/:id', function (req, res, next) {
    models.Teams.update({
            name: req.body.name,
            phone: req.body.phone
        },
        {where: {id: req.params.id}}).then(function (data) {
        res.status(200).send();
    }).catch(function () {
        res.status(404).send();
    });
});
// Add new team
router.post('/', function (req, res) {
    models.Teams.create({name: req.body.name, phone: req.body.phone}).then(function (data) {
        models.Teams.find({
            where: {id: data.id},
            include: [
                {model: models.TeamPhotos, as: 'photos', attributes: ['path', 'year']},
                {model: models.Groups, as: 'presence', attributes: ['year']}
            ],
            order: [
                [{model: models.TeamPhotos, as: 'photos'}, 'year', 'ASC'],
                [{model: models.Groups, as: 'presence'}, 'year', 'ASC']
            ]
        }).then(function (data) {
            let _data = JSON.parse(JSON.stringify(data));
            _data.presence = pluck(_data.presence, "year");

            res.status(201).send(_data);
        }).catch(function () {
            res.status(500).send();
        });
    }).catch(function () {
        res.status(400).send();
    });
});
// Delete team
router.delete('/:id', function (req, res, next) {
    models.Teams.destroy({where: {id: req.params.id}}).then(function (data) {
        res.status(200).send();
    }).catch(function () {
        res.status(500).send();
    });
});
// Upload photo of team
router.post('/:id/photo', function (req, res, next) {
    upload(req, res, function (err) {
        var photo = fs.readFileSync(photoStorage + '\\' + req.file.originalname);
        var photoType = fileType(photo);
        if (err) {
            res.status(500).send(err);
        } else if (photoType != null && (!photoType.mime.localeCompare('image/jpeg') || !photoType.mime.localeCompare('image/pjpeg'))) {
            models.TeamPhotos.create({
                path: req.file.originalname,
                team_id: req.params.id,
                year: req.headers['photo-year']
            }).then(function (data) {
                res.status(200).send();
            });
        } else {
            fs.unlinkSync(photoStorage + '\\' + req.file.originalname);
            res.status(500).send();
        }
    })
});
// Delete photo of team
router.delete('/:id/photo', function (req, res, next) {
    models.TeamPhotos.destroy({where: {team_id: req.params.id, path: req.query.path}}).then(function (data) {
        fs.unlink(photoStorage + '/' + req.query.path, function (err) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send();
            }
        });
    }).catch(function () {
        res.status(500).send();
    });
});

module.exports = router;
