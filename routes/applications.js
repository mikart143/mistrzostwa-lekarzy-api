var models = require('../models');
var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    models.Applications.findAll({
        attributes:{
            include:[
                [models.Sequelize.literal('team.name'),'team_name']
            ]
        },
        include: [
            {model: models.Teams, as:'team', attributes:[]},
            {model: models.Players, as:'players', attributes:{ exclude:['team_id']}}
        ],
        order: [
            [{model: models.Players, as:'players'},'name','ASC']
        ]
    }).then(function (data) {
        res.status(200).send(data);
    }).catch(function () {
        res.status(500).send();
    });
});

module.exports = router;
